﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Zenject;
using SimpleJSON;

namespace VReddo.SkeeBall
{
    public class HighScoreService : IInitializable, ILateDisposable
    {
        public string highScoreJson = "";
        public string playerDataToSend = "";
        public string tokenStr = "";
        public Action OnFetchedData = null;

        private GameManager gameManager;

        public void Initialize()
        {
            //gameManager.OnEndGame += SendScoreToServer;
        }

        public void LateDispose()
        {
            //gameManager.OnEndGame -= SendScoreToServer;
        }

        [Inject]
        public void Constructor(GameManager _gameManager)
        {
            gameManager = _gameManager;
        }

        // Get Token
        public IEnumerator GetToken(string uri)
        {
            using (UnityWebRequest webRequest = UnityWebRequest.Put(uri, SerializePlayerData()))
            {
                webRequest.method = "POST";
                webRequest.SetRequestHeader("Content-Type", "application/json");
                yield return webRequest.SendWebRequest();

                if (webRequest.isNetworkError)
                    Debug.Log(webRequest.error);
                else
                {
                    Debug.Log("POST successful!" + webRequest.downloadHandler.text);
                    var data = JSON.Parse(webRequest.downloadHandler.text);
                    tokenStr = data["payload"];
                    gameManager.UpdateScoreBoard();
                }
            }
        }

        // Get High Score Data
        public IEnumerator GetHighScore(string uri)
        {
            using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
            {
                // Request and wait for the desired page.
                webRequest.SetRequestHeader("Content-Type", "application/json");
                webRequest.SetRequestHeader("Authorization", "Bearer " + tokenStr);
                yield return webRequest.SendWebRequest();

                string[] pages = uri.Split('/');
                int page = pages.Length - 1;
                string playerData = null;

                if (webRequest.isNetworkError)
                    Debug.Log(pages[page] + ": Error: " + webRequest.error);
                else
                {
                    Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                    playerData = webRequest.downloadHandler.text;
                    DeserializeData(playerData);
                }
            }
        }

        // Post High Score Data
        public IEnumerator PostRequest(string uri)
        {
            using (UnityWebRequest webRequest = UnityWebRequest.Put(uri, SerializeGameData()))
            {
                webRequest.method = "POST";
                webRequest.SetRequestHeader("Content-Type", "application/json");
                webRequest.SetRequestHeader("Authorization", "Bearer " + tokenStr);
                yield return webRequest.SendWebRequest();

                if (webRequest.isNetworkError)
                    Debug.Log(webRequest.error);
                else
                {
                    Debug.Log("POST successful!" + webRequest.downloadHandler.text);
                    gameManager.UpdateScoreBoard();
                }
            }
        }

        // Serialize Player Data to get token
        // HARD CODED for the meantime
        private string SerializePlayerData()
        {
            string playerDataStr = "";
            PlayerData playerData = new PlayerData();
            playerData.uname = "admin";
            playerData.email = "henry.i@cfeduex.com";
            playerData.pword = "Redmako2018";
            playerDataStr = JsonUtility.ToJson(playerData);

            return playerDataStr;
        }

        // Serialize data
        private string SerializeGameData()
        {
            GameData gameData = new GameData();
            gameData.gameid = 3;
            switch (gameManager.curPlayerName)
            {
                case "Emer":
                    gameData.userid = 1;
                    break;
                case "Henry":
                    gameData.userid = 2;
                    break;
                case "JM":
                    gameData.userid = 3;
                    break;
                case "Pao":
                    gameData.userid = 4;
                    break;
                case "Zeus":
                    gameData.userid = 5;
                    break;
                case "Axle":
                    gameData.userid = 6;
                    break;
            }
            gameData.score = gameManager.score;

            playerDataToSend = JsonUtility.ToJson(gameData);
            Debug.Log("Serialized Data: " + playerDataToSend);

            return playerDataToSend;
        }

        // Force Setters for player 
        private void DeserializeData(string jsonData)
        {
            highScoreJson = ""; 
            var data = JSON.Parse(jsonData);
            int payloadCount = data["payload"].Count;

            for (int i = 0; i < payloadCount; i++)
            {
                highScoreJson += i + 1 + "\t\t";
                switch(data["payload"][i]["userid"].Value)
                {
                    case "1":
                        highScoreJson += "Emer:  \t";
                        break;
                    case "2":
                        highScoreJson += "Henry: \t";
                        break;
                    case "3":
                        highScoreJson += "JM:    \t\t";
                        break;
                    case "4":
                        highScoreJson += "Pao:   \t";
                        break;
                    case "5":
                        highScoreJson += "Zeus:  \t";
                        break;
                    case "6":
                        highScoreJson += "Axle:  \t";
                        break;
                }
                highScoreJson += data["payload"][i]["score"] + "\n";
            }

            Debug.Log("Deserialized Data: " + highScoreJson);

            if (OnFetchedData != null)
                OnFetchedData();
        }
    }

    [System.Serializable]
    public class GameData
    {
        public int gameid;
        public int userid;
        public int score;
    }

    [System.Serializable]
    public class PlayerData
    {
        public string uname;
        public string email;
        public string pword;
    }
}
