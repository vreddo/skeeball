﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.XR;
using TMPro;

public class LoadAssetBundles : MonoBehaviour
{
    public string prefabPath;
    public uint crcString;
    public string envPrefabName;
    public string sunBGPrefabName;
    public string bgmPrefabName;
    //public GameObject loadingText;
    public GameObject startBtn;
    public TextMeshProUGUI loadingTxt;

    private UnityWebRequest www;
    private float m_CurrentValue;
    private bool isDownloaded = false;

    void Start()
    {
        //XRSettings.eyeTextureResolutionScale = 1.2f;
        StartCoroutine("GetPrefabAssetBundle");
    }

    IEnumerator GetPrefabAssetBundle()
    {
        using (www = UnityWebRequestAssetBundle.GetAssetBundle(prefabPath, 0, crcString))
        {
            // wait for load to finish
            yield return www.SendWebRequest();
            if (www.error != null)
            {
                Debug.LogError("www error: " + www.error);
                yield break;
            }
            isDownloaded = true;

            // get bundle from downloadhandler
            var bundle = ((DownloadHandlerAssetBundle)www.downloadHandler).assetBundle;
            var envPrefab = bundle.LoadAsset(envPrefabName);
            var sunPrefab = bundle.LoadAsset(sunBGPrefabName);
            //var bgmPrefab = bundle.LoadAsset(bgmPrefabName);

            loadingTxt.gameObject.SetActive(false);
            startBtn.SetActive(true);
            Instantiate(envPrefab);
            Instantiate(sunPrefab);
            //Instantiate(bgmPrefab);
        }
    }

    void Update()
    {
        if (!isDownloaded)
        {
            m_CurrentValue = Mathf.RoundToInt(www.downloadProgress * 100);
            //Debug.Log(www.downloadProgress + " | " +m_CurrentValue);
            loadingTxt.text = "Loading (" + m_CurrentValue + "%)";
        }
    }
}
