﻿using UnityEngine;
using TMPro;
using Zenject;

namespace VReddo.SkeeBall
{
    public class DummyUI : MonoBehaviour
    {
        public TextMeshProUGUI playerNameText;

        private GameManager gameManager;
        private SFXManager sfxManager;
        private string strPlayerName = "Emer";

        void Start()
        {
            SetPlayerName(strPlayerName);
        }

        [Inject]
        public void Constructor(GameManager _gameManager, SFXManager _sfxManager)
        {
            gameManager = _gameManager;
            sfxManager = _sfxManager;
        }

        public void SetPlayerName(string _playername)
        {
            strPlayerName = _playername;
            gameManager.curPlayerName = strPlayerName;
            playerNameText.text = strPlayerName;
            //sfxManager.PlayClip(SFXClips.BUTTON_CLICK);
        }
    }
}
