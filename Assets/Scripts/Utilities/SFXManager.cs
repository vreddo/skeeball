﻿using UnityEngine;

namespace VReddo.SkeeBall
{
    public enum SFXClips
    {
        SCORE,
        BALL_ROLL,
        BUTTON_CLICK
    }

    public class SFXManager : MonoBehaviour
    {
        public AudioClip[] sfxClip;
        [SerializeField] private AudioSource[] audioSource;

        #region Public Methods
        public void PlayClip(SFXClips clip)
        {
            switch (clip)
            {
                case SFXClips.SCORE:
                    audioSource[0].Play();
                    break;
                case SFXClips.BALL_ROLL:
                    audioSource[1].Play();
                    break;
                case SFXClips.BUTTON_CLICK:
                    audioSource[2].clip = sfxClip[0];
                    audioSource[2].Play();
                    break;
            }
        }

        public void StopClip(SFXClips clip)
        {
            switch (clip)
            {
                case SFXClips.SCORE:
                    audioSource[0].Stop();
                    break;
                case SFXClips.BALL_ROLL:
                    audioSource[1].Stop();
                    break;
                case SFXClips.BUTTON_CLICK:
                    audioSource[2].Stop();
                    break;
            }
        }
        #endregion
    }
}
