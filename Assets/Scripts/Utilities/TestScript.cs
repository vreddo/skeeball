﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class TestScript : MonoBehaviour
{
    //private AssetBundleCreateRequest myLoadedAssetBundle;
    private AssetBundle myLoadedAssetBundle;
    public string path;
    public string prefabName;

    public string url;
    private AssetBundle sceneTest;

    void Start()
    {

    }

    public void OnBtnPressed()
    {
        LoadAssetBundle(path);
        //myLoadedAssetBundle = AssetBundle.LoadFromFileAsync(Path.Combine(Application.streamingAssetsPath, "test"));
        //if (myLoadedAssetBundle == null)
        //{
        //    Debug.Log("Failed to load AssetBundle!");
        //    return;
        //}
        //var prefab = myLoadedAssetBundle.LoadAsset<GameObject>("MyObject");
        //Instantiate(prefab);

        //myLoadedAssetBundle.Unload(false);

        sceneTest = AssetBundle.LoadFromFile(url);
        Debug.Log(sceneTest == null ? " Failed to load AssetBundle" : " AssetBundle succesfully loaded");


        string[] scenes = sceneTest.GetAllScenePaths();
        string scene = Path.GetFileNameWithoutExtension(scenes[0]);
        SceneManager.LoadScene(scene);
    }

    //IEnumerator Test()
    //{
    //    using (UnityWebRequest www = new UnityWebRequest(url))
    //    {
    //        yield return www;
    //        if (!string.IsNullOrEmpty(www.error))
    //        {
    //            Debug.LogError(www.error);
    //            yield break;
    //        }

    //        sceneTest = www.sceneTest;
    //    }
    //}

    private void LoadAssetBundle(string bundleUrl)
    {
        myLoadedAssetBundle = AssetBundle.LoadFromFile(bundleUrl);

        Debug.Log(myLoadedAssetBundle == null ? " Failed to load AssetBundle" : " AssetBundle succesfully loaded");
        if (myLoadedAssetBundle != null)
            InstantiateObject(prefabName);
    }

    private void InstantiateObject(string assetName)
    {
        var prefab = myLoadedAssetBundle.LoadAsset(assetName);
        Instantiate(prefab);
    }

    private void Update()
    {
        //if (myLoadedAssetBundle == null)
        //    return;

        //if (myLoadedAssetBundle.isDone)
        //{
        //    {
        //        SceneManager.LoadScene("GameScene");
        //    }
        //}
    }


}
