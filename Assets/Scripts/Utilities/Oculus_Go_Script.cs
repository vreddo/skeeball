﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

// Simply place this on the GameObject that holds the OVRmanager for the Oculus Go user.

public class Oculus_Go_Script : MonoBehaviour
{
    public bool useLMS = true;
    public bool useHigherRefresh = true;

    public bool OverrideResScale = true;
    [Range(0.25f, 2)]
    public float OverrideScale = 1.25f;

    private bool hasApplied = false;

	// Use this for initialization
	void Start ()
    {
        if(this.GetComponent<OVRManager>())
        {
            if (XRDevice.model == "Oculus Go")
            {
                if (useLMS)
                {
                    OVRManager.tiledMultiResLevel = OVRManager.TiledMultiResLevel.LMSHigh;
                }
                if (useHigherRefresh)
                {
                    QualitySettings.vSyncCount = 0;
                    OVRManager.display.displayFrequency = 72.0f;
                    Application.targetFrameRate = 72;
                }
            }

            hasApplied = true;
        }
        else
        {
            hasApplied = false;
        }
    }

    private void LateUpdate()
    {
        if(XRDevice.model == "Oculus Go")
        {
            if(this.GetComponent<OVRManager>() && !hasApplied)
            {
                if (useLMS)
                {
                    OVRManager.tiledMultiResLevel = OVRManager.TiledMultiResLevel.LMSHigh;
                }
                if (useHigherRefresh)
                {
                    QualitySettings.vSyncCount = 0;
                    OVRManager.display.displayFrequency = 72.0f;
                    Application.targetFrameRate = 72;
                }
                hasApplied = true;
            }

            if (OverrideResScale && XRSettings.eyeTextureResolutionScale != OverrideScale)
            {
                XRSettings.eyeTextureResolutionScale = OverrideScale;
            }
        }
    }
}