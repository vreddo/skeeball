﻿using UnityEngine;
using Zenject;

namespace VReddo.SkeeBall
{
    public class BallScript : MonoBehaviour, IPoolable<float, IMemoryPool>
    {
        [HideInInspector] public bool isFired = false;

        private GameManager gameManager;
        private Rigidbody rb;
        private IMemoryPool pool;
        private float startTime;
        private float lifeTime;
        private float timer = 0f;

        #region Monobehavior Methods
        void Awake()
        {
            rb = GetComponent<Rigidbody>();
        }

        void Update()
        {
            if (isFired)
            {
                timer += Time.deltaTime;
                if (timer >= lifeTime)
                    OnPoolDespawn();
            }
            else if (gameManager.gameState == GameManager.GameState.GAME_ENDED && !isFired)
                OnPoolDespawn();
        }
        #endregion

        #region Public Methods
        [Inject]
        public void Construct(GameManager _gameManager)
        {
            gameManager = _gameManager;
        }

        public void OnPoolDespawn()
        {
            pool.Despawn(this);
        }

        public void OnFireUpBall(float power)
        {
            isFired = true;
            rb.AddForce(transform.forward * power * 20);
        }

        public void OnDespawned()
        {
            gameManager.ballsFiredCount--;
            timer = 0;
            isFired = false;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = new Vector3(0.0f, 0.0f, 0.0f);

            pool = null;
        }

        public void OnSpawned(float _lifeTime, IMemoryPool _pool)
        {
            gameManager.ballsFiredCount++;
            pool = _pool;
            lifeTime = _lifeTime;

            //startTime = Time.realtimeSinceStartup;
        }
        #endregion

        public class Factory : PlaceholderFactory<float, BallScript>
        {
        }
    }
}
