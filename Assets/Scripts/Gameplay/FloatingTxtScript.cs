﻿using UnityEngine;
using Zenject;

namespace VReddo.SkeeBall
{
    public class FloatingTxtScript : MonoBehaviour, IPoolable<float, IMemoryPool>
    {
        #region Public Variables
        public float destroyTime = 0;
        //public Vector3 offset = new Vector3(0, 0.1f, 0);
        #endregion

        #region Private Variables
        private Vector3 initPos;
        private IMemoryPool pool;
        private float startTime;
        private float lifeTime;
        #endregion

        #region Zenject Methods
        public void OnDespawned()
        {
            //transform.SetParent(null);
            pool = null;
        }

        public void OnSpawned(float _lifeTime, IMemoryPool _pool)
        {
            pool = _pool;
            lifeTime = _lifeTime;

            startTime = Time.realtimeSinceStartup;
        }
        #endregion

        #region Monobehaviour Methods
        void Update()
        {
            if (Time.realtimeSinceStartup - startTime > lifeTime)
            {
                pool.Despawn(this);
            }
        }
        #endregion

        public class Factory : PlaceholderFactory<float, FloatingTxtScript>
        {
        }
    }
}
