﻿using UnityEngine;
using TMPro;
using Zenject;

namespace VReddo.SkeeBall
{
    public class HighscoreDisplay : MonoBehaviour
    {
        public TextMeshProUGUI highScoreText;

        private GameManager gameManager;
        private HighScoreService highScoreService;

        #region Monobehaviour Methods
        void OnEnable()
        {
            highScoreService.OnFetchedData += OnFetchedData;
        }

        void OnDisable()
        {
            highScoreService.OnFetchedData -= OnFetchedData;
        }

        void Start()
        {
            gameManager.GetToken();
        }
        #endregion

        [Inject]
        public void Constructor(GameManager _gameManager, HighScoreService _highScoreService)
        {
            gameManager = _gameManager;
            highScoreService = _highScoreService;
        }

        private void OnFetchedData()
        {
            highScoreText.text = highScoreService.highScoreJson;
        }
    }  
}
