﻿using UnityEngine;
using Zenject;

namespace VReddo.SkeeBall
{
    public class CollisionChecker : MonoBehaviour
    {
        private SFXManager sfxManager;
        [SerializeField] private SFXClips sfxClip;

        #region Monobehaviour Methods
        void OnCollisionEnter(Collision collision)
        {
            sfxManager.PlayClip(sfxClip);
        }

        void OnCollisionExit(Collision collision)
        {
            sfxManager.StopClip(sfxClip);
        }
        #endregion

        #region Public Methods
        [Inject]
        public void Constructor(SFXManager _sfxManager)
        {
            sfxManager = _sfxManager;
        }
        #endregion
    }
}
