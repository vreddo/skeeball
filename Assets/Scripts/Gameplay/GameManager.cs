﻿using UnityEngine;
using TMPro;
using Zenject;

namespace VReddo.SkeeBall
{
    [RequireComponent(typeof(BallSpawner))]
    [RequireComponent(typeof(TimerScript))]
    public class GameManager : MonoBehaviour
    {
        public enum GameState
        {
            INIT,
            START_GAME,
            GAME_ENDED
        }
        public int score = 0;
        public int ballsFiredCount = 0;
        public float gameTime;
        public string curPlayerName = ""; // For testing of highscore only
        public GameState gameState;

        // Anchors
        public GameObject m_LeftAnchor;
        public GameObject m_RightAnchor;
        public GameObject m_HeadAnchor;
 
        [SerializeField] private TextMeshPro scoreText;
        [SerializeField] private TextMeshPro timerText;
        [SerializeField] private TextMeshProUGUI resultText;
        [SerializeField] private TextMeshProUGUI fpsText;

        [SerializeField] private GameObject resultPanel;
        [SerializeField] private GameObject startPanel;
        [SerializeField] private GameObject floatingTextSpawnPoint;
        [SerializeField] private GameObject vrCamera;

        private bool isResultShown = false;
        private BallSpawner ballSpawner;
        private SFXManager sfxManager;
        private TimerScript timer;

        private PlayerEvents playerEvents;
        private FloatingTxtScript.Factory floatingTextFactory;
        private HighScoreService highScoreService;

        private float deltaTime = 0.0f; //FPS

        #region Monobehaviour Methods
        void OnEnable()
        {
            playerEvents.OnControllerSource += UpdateOrigin;
            PointsArea.OnGivePoints += OnGivePoints;
        }

        void OnDisable()
        {
            playerEvents.OnControllerSource -= UpdateOrigin;
            PointsArea.OnGivePoints -= OnGivePoints;
        }

        void Awake()
        {
            gameState = GameState.INIT;
            ballSpawner = GetComponent<BallSpawner>();
            timer = GetComponent<TimerScript>();
        }

        void Start()
        {
            Reset();
        }

        void Update()
        {
            deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;

            float msec = deltaTime * 1000.0f;
            float fps = 1.0f / deltaTime;
            string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);

            fpsText.text = text;

            if (gameState == GameState.GAME_ENDED && ballsFiredCount == 0)
            {
                if (!isResultShown)
                    ShowResult();
            }
        }
        #endregion

        #region Public Methods
        [Inject]
        public void Constructor(SFXManager _sfxManager, 
            PlayerEvents _playerEvents, 
            FloatingTxtScript.Factory _floatingTextFactory,
            HighScoreService _highScoreService)
        {
            sfxManager = _sfxManager;
            playerEvents = _playerEvents;
            floatingTextFactory = _floatingTextFactory;
            highScoreService = _highScoreService;
        }

        // Invoked on Start button press
        public void OnStartGame()
        {
            startPanel.SetActive(false);
            timer.StartCountingDown();
            gameState = GameState.START_GAME;
            sfxManager.PlayClip(SFXClips.BUTTON_CLICK);
        }

        // Invoked on Restart button press
        public void OnRestart()
        {
            resultPanel.SetActive(false);
            timer.StartCountingDown();
            Reset();

            gameState = GameState.START_GAME;
            sfxManager.PlayClip(SFXClips.BUTTON_CLICK);
        }

        // Updates time text on world UI
        public void UpdateTime(float time)
        {
            timerText.text = time.ToString();
            if (time <= 0)
                gameState = GameState.GAME_ENDED;
        }

        // Get Token
        public void GetToken()
        {
            StartCoroutine(highScoreService.GetToken("http://192.168.1.253:9090/api/gettoken/"));
        }

        // Updates highScoreBoard
        public void UpdateScoreBoard()
        {
            StartCoroutine(highScoreService.GetHighScore("http://192.168.1.253:9090/api/topten/3"));
        }

        // Upload scores to server
        public void UploadScore()
        {
            StartCoroutine(highScoreService.PostRequest("http://192.168.1.253:9090/api/highscores/"));
        }

        #endregion

        #region Private Methods
        // Invoked on player score
        private void OnGivePoints(int points)
        {
            ShowFloatingText(points);
            score += points;
            UpdateScoreText();
        }

        // Displays the score
        private void UpdateScoreText()
        {
            scoreText.text = score.ToString();
        }

        // Displays the result screen
        private void ShowResult()
        {
            if (isResultShown)
                return;

            isResultShown = true;
            resultPanel.SetActive(true);
            resultText.text = "Your Score: " + score.ToString();

            UploadScore();
        }

        // Displays floating text when player scores
        private void ShowFloatingText(int points)
        {
            var go = floatingTextFactory.Create(1f);
            go.transform.SetParent(floatingTextSpawnPoint.transform);
            go.transform.position = floatingTextSpawnPoint.transform.position;
            go.transform.rotation = floatingTextSpawnPoint.transform.rotation;
            go.GetComponent<TextMeshPro>().text = "+" + points;
        }

        // Sets camera position based on players controller settings
        private void UpdateOrigin(OVRInput.Controller controller, GameObject controllerObject)
        {
            if (controller == OVRInput.Controller.LTrackedRemote)
                vrCamera.gameObject.transform.position = new Vector3(0.21f, 1.328f, -5.774f);
            else if (controller == OVRInput.Controller.RTrackedRemote)
                vrCamera.gameObject.transform.position = new Vector3(-0.208f, 1.328f, -5.774f);
        }

        // Reset Values of the text fields
        private void Reset()
        {
            isResultShown = false;
            score = 0;
            UpdateScoreText();
            UpdateTime(gameTime);
        }
        #endregion
    }
}
