﻿using UnityEngine;

namespace VReddo.SkeeBall
{
    public class Despawner : MonoBehaviour
    {
        #region Monobehaviour Methods
        void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Ball"))
                collision.gameObject.GetComponent<BallScript>().OnPoolDespawn();
        }
        #endregion
    }
}
