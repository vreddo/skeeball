﻿using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace VReddo.SkeeBall
{
    public class PointsArea : MonoBehaviour
    {
        public static UnityAction<int> OnGivePoints;
        public int points;

        private SFXManager sfxManager;
        private BallScript.Factory ballFactory;

        #region Monobehaviour Methods
        void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Ball"))
            {
                if (OnGivePoints != null)
                    OnGivePoints(points);
                sfxManager.PlayClip(SFXClips.SCORE);

                collision.gameObject.GetComponent<BallScript>().OnPoolDespawn();
                //Destroy(collision.gameObject);
            }
        }
        #endregion

        #region Public Methods
        [Inject]
        public void Constructor(SFXManager _sfxManager, BallScript.Factory _ballFactory)
        {
            sfxManager = _sfxManager;
            ballFactory = _ballFactory;
        }
        #endregion
    }
}