﻿using System.Collections;
using UnityEngine;
using TMPro;
using Zenject;

namespace VReddo.SkeeBall
{
    public class BallSpawner : MonoBehaviour
    {
        public GameObject spawnPoint;
        public TextMeshPro testText;
        public GameObject rightHandSpawnPoint;
        public GameObject leftHandSpawnPoint;

        private BallScript.Factory ballFactory;
        private BallScript ball;
        private PlayerEvents playerEvents;
        private GameManager gameManager;
        private OVRInput.Controller controller;
        private float power = 0;
        private bool isRightHanded;
        private bool isTriggerDown = false;
        private IEnumerator corout = null;

        #region Monobehaviour Methods
        void OnEnable()
        {
            playerEvents.OnIndexTriggerDown += OnIndexTriggerDown;
            playerEvents.OnIndexTriggerUp += OnIndexTriggerUp;
        }

        void OnDisable()
        {
            playerEvents.OnIndexTriggerDown -= OnIndexTriggerDown;
            playerEvents.OnIndexTriggerUp -= OnIndexTriggerUp;
        }

        void Update()
        {
            if (gameManager.gameState == GameManager.GameState.GAME_ENDED ||
               gameManager.gameState == GameManager.GameState.INIT)
                return;

            //Debug.Log("OVRInput.GetActiveController: " + OVRInput.GetActiveController());

            if (OVRInput.IsControllerConnected(OVRInput.Controller.RTrackedRemote))
            {
                isRightHanded = true;
                controller = OVRInput.Controller.RTrackedRemote;
            }

            if (OVRInput.IsControllerConnected(OVRInput.Controller.RTouch))
            {
                isRightHanded = true;
                controller = OVRInput.Controller.RTouch;
            }

            if (OVRInput.IsControllerConnected(OVRInput.Controller.LTrackedRemote))
            {
                isRightHanded = false;
                controller = OVRInput.Controller.LTrackedRemote;
            }

            if (isTriggerDown)
            {
                if (OVRInput.GetLocalControllerAcceleration(controller).y > power)
                    power = OVRInput.GetLocalControllerAcceleration(controller).y;

                corout = PowerReset();
                StartCoroutine(corout);

                testText.text = "Power: " + power;
            }
            else
            {
                if (corout != null)
                    StopCoroutine(corout);
            }

            // For PC Simulation Only
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (gameManager.gameState == GameManager.GameState.GAME_ENDED ||
                    gameManager.gameState == GameManager.GameState.INIT)
                    return;

                ball = ballFactory.Create(5f);
                ball.transform.position = spawnPoint.transform.position;
                ball.transform.rotation = spawnPoint.transform.rotation;
                ball.GetComponent<BallScript>().OnFireUpBall(20);
            }
        }
        #endregion

        #region Public Methods
        [Inject]
        public void Construct(GameManager _gameManager, 
            PlayerEvents _playerEvents, 
            BallScript.Factory _ballFactory)
        {
            gameManager = _gameManager;
            playerEvents = _playerEvents;
            ballFactory = _ballFactory;
        }
        #endregion

        #region Private Methods
        // Called when player presses the trigger button on the remote
        private void OnIndexTriggerDown()
        {
            if (gameManager.gameState == GameManager.GameState.GAME_ENDED || 
                gameManager.gameState == GameManager.GameState.INIT)
                return;

            ball = ballFactory.Create(5f);

            // Spawns ball on hand
            if (isRightHanded)
            {
                ball.transform.SetParent(rightHandSpawnPoint.transform);
                ball.transform.position = rightHandSpawnPoint.transform.position;
                ball.transform.rotation = rightHandSpawnPoint.transform.rotation;
            }
            else
            {
                ball.transform.SetParent(leftHandSpawnPoint.transform);
                ball.transform.position = leftHandSpawnPoint.transform.position;
                ball.transform.rotation = leftHandSpawnPoint.transform.rotation;
            }

            ball.GetComponent<Rigidbody>().useGravity = false;
            isTriggerDown = true;
        }

        // Called when player releases the trigger button on the remote
        private void OnIndexTriggerUp()
        {
            if (ball == null || gameManager.gameState == GameManager.GameState.GAME_ENDED)
                return;

            // Release the ball
            ball.transform.SetParent(null);
            ball.GetComponent<Rigidbody>().useGravity = true;
            ball.GetComponent<BallScript>().OnFireUpBall(power);

            power = 0;
            isTriggerDown = false;
        }

        // Resets the power to zero after 0.5 secs
        IEnumerator PowerReset()
        {
            yield return new WaitForSeconds(0.5f);
            power = 0;
            corout = null;
        }
        #endregion
    }
}
