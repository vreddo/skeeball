using UnityEngine;
using Zenject;

namespace VReddo.SkeeBall
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private SFXManager sfxManager;
        public GameObject ballGO;
        public GameObject floatingTxtGO;

        public override void InstallBindings()
        {
            Container.Bind<GameManager>().FromInstance(gameManager);
            Container.Bind<SFXManager>().FromInstance(sfxManager);
            Container.BindInterfacesAndSelfTo<PlayerEvents>().AsSingle();
            Container.BindInterfacesAndSelfTo<HighScoreService>().AsSingle();
 
            Container.BindFactory<float, BallScript, BallScript.Factory>()
                .FromPoolableMemoryPool<float, BallScript, BallPool>
                (poolBinder => poolBinder.WithInitialSize(15).FromComponentInNewPrefab(ballGO).
                UnderTransformGroup("Balls"));

            Container.BindFactory<float, FloatingTxtScript, FloatingTxtScript.Factory>()
                .FromPoolableMemoryPool<float, FloatingTxtScript, FloatingTextPool>
                (poolBinder => poolBinder.WithInitialSize(10).FromComponentInNewPrefab(floatingTxtGO)
                .UnderTransformGroup("FloatingText"));
        }
    }

    public class BallPool : MonoPoolableMemoryPool<float, IMemoryPool, BallScript>
    {
    }

    public class FloatingTextPool : MonoPoolableMemoryPool<float, IMemoryPool, FloatingTxtScript>
    {
    }
}