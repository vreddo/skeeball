﻿using UnityEngine;

namespace VReddo.SkeeBall
{
    public class Interactable : MonoBehaviour
    {
        public void Pressed()
        {
            MeshRenderer renderer = GetComponent<MeshRenderer>();
            bool flip = !renderer.enabled;

            renderer.enabled = flip;
        }
    }
}
