How to Play (VR):
Game is based on the classic arcade game Skee Ball.
- To spawn a ball press and hold the Index Trigger Button.
- Generate power by swinging the controller forward at your desired direction. Then release the Index Trigger Button at the peak of the swing.
- Player is given 60 seconds to score as many points as possible.

Getting Started:
Run on Unity Editor
- EventSystem gameobject on the hierachy should have OVR Input Module deactivated and Standalone Input Module activated.
- Canvas gameobject on the hierachy should have OVR Raycaster deactivated and Graphic Raycaster activated.
- Use Space Bar to spawn a ball on the play area.
Notes:
- This game is not configured well to be played on PC/Windows Build. There is no power simulation and direction for this build.

Run on VR
- EventSystem gameobject on the hierachy should have OVR Input Module activated and Standalone Input Module deactivated.
- Canvas gameobject on the hierachy should have OVR Raycaster activated and Graphic Raycaster deactivated.
- Switch to android platform on the Build Settings.
- On the PlayerSettings->XR Setting, make sure that virtual reality is marked check and Oculus is added to VR SDK.
- On the PlayerSettings->Other Settings, Set Minimum API Level to API Level 21 'Lollipop'.

Built with:
Unity 2018.3.5.f1

Other Tools used:
Zenject
Oculus